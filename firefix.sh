#!/bin/bash -i

# Save custom user css to /tmp/userChrome.css to disable the expanding
# urlbar introduced in Firefox 75.0
# Thanks to reddit user /u/It_Was_The_Other_Guy for creating it.
wget "https://raw.githubusercontent.com/MrOtherGuy/firefox-csshacks\
/master/chrome/urlbar_popup_full_width.css" -O /tmp/userChrome.css

# Get paths of user profile directories
profiles=$(grep -E 'Path=.*' < ~/.mozilla/firefox/profiles.ini |
	       awk -F '=' '{printf $2"\n"}')

# Run through profiles copying the custom user css from /tmp
# to each profile
for profile in $profiles
do
    profile=~/.mozilla/firefox/$profile
    mkdir $profile/chrome
    cp -n /tmp/userChrome.css $profile/chrome
done

# Copy the autoconfig scripts to the appropriate directories
sudo cp autoconfig.js /usr/lib/firefox/defaults/pref/
sudo cp firefox.cfg /usr/lib/firefox/
