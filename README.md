# firefix

Setup script to make Firefox behave the way I want it to.

* Pulls in a custom [userChrome](https://www.userchrome.org/) from https://github.com/MrOtherGuy/firefox-csshacks/blob/master/chrome/urlbar_full_width.css to force
Firefox to use the old-style url-bar before version 75.0

* Uses [autoconfig](https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig) to set a whole bunch of preferences to make Firefox better suited
to my workflow.

## Autoconfig Changes:

* Enable custom userChrome

* Disable warning when closing multiple tabs

* Enable autoscrolling

* Disable C-Tab cycling in recently-used order

* Keep the search-box as a separate bar

* Disable recommending features

* Disable pocket

* Disable search suggestions

* Enable always sending a do-not-track header

* Disable form autofill

* Disable the built-in password manager
